import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

const MuiDltUsr = ({ openDeleteModal, handleCloseDlt, data }) => {

// MUI MODAL STYLE
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 3,
    borderRadius: '10px',
};
// MUI MODAL STYLE

    return (
        <Modal
            open={openDeleteModal}
            onClose={handleCloseDlt}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
            <div className='text-center'>
                <p className='p-3'>
                    Do you really want to delete this record? This process cannot be undone.
                </p>

                <div className="justify-content-between">
                    <Button variant="outlined" onClick={handleCloseDlt}>Cancel</Button> &nbsp;
                    <Button variant="contained" color="error" >Delete</Button>
                </div>
            </div>
            </Box>
        </Modal>
    )
}

export default MuiDltUsr